<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Repository\ProjectRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Project;
use App\Service\FileService;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * @Route("/api/project", name="project")
 */
class ProjectController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }


      /**
     * @Route("/", methods={"GET"})
     */
    public function all(ProjectRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name', 'description', 'image', 'link']]);

        $response = JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));
        return $response;
    }

/*

    public function add(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $project = $this->serializer->deserialize($content, Project::class, "json");

        $manager->persist($project);
        $manager->flush();

        $data = $this->serializer->normalize($project, null, ['attributes' =>['id', 'name']]);

        $response = new Response($this->serializer->serialize($project, "json"));
        return $response;
    }
*/
    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $req, FileService $fileService) {
        $description = json_decode($req->getContent(),true);
        //On récupère le fichier dans la request
        $image = $req->files->get("img");
        //On récupère l'url absolue de notre application
        $absoluteUrl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
        //On utilise le fileService pour uploader l'image
        $imageUrl = $fileService->upload($image, $absoluteUrl);

        $project = new Project();
        $project->setName($req->get("name"));
        $project->setDescription($req->get("description"));
        $project->setLink($req->get("link"));
        //pareil qu'au dessus mais pour link
        $project->setImage($imageUrl);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($project);
        $manager->flush();

        $data = $this->serializer->normalize($project, null, ['attributes'=>['id', 'image','name', 'description']]);

        $response = new Response($this->serializer->serialize($data, "json"));

        return $response;


    }


      /**
     * @Route("/", methods={"DELETE"})
     */
    public function del(Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($project);
        $manager->flush();

        return new Response("OK", 204);
    }

        /**
     * @Route("/", methods={"PUT"})
     */
    public function upd(Request $request, Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, Project::class, "json");

        $project->setName($update->getName());

        $manager->persist($project);
        $manager->flush();

        $data = $this->serializer->normalize($oneproject, null, ['attributes' => ['id', 'name']]);        

        $response = new Response($this->serializer->serialize($project, "json"));
        return $response;
    }


}
